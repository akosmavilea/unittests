@title[Title slide]

# Unit Testing

---

@title[Introduction]

## Software Quality Management

- Too large subject
- Need to start somewhere
- Focus on unit testing for the moment

Note: Software testing is a complex and vast subject, and it would be ridiculous to pretend to be able to tackle all of it in just one hour.

---

## Software Quality Factors

- Internal
- External

Note: Internal quality factors are a concern for developers and anyone involved in the creation of a work of software (managers, testers, etc.) External quality factors, on the other hand, are publicly visible and have a direct impact on end users.

---

## Internal Quality Factors

- Maintainability
- Flexibility
- Portability
- Reusability
- Readability
- Testability
- Understandability

Note: These are usually perceived as crytical by developers.

---

## External Quality Factors

- Correctness
- Usability
- Efficiency
- Reliability
- Integrity
- Adaptability
- Accuracy
- Robustness

Note: These are of concern for end users, investors, but also for developers.

---

## Tension

![](assets/images/cc2e-quality-factors.png)

Note: When trying to increase one of the external quality factors, others might suffer in the process. For example, increasing the efficiency of the software (that is, making it consume less memory or running faster) might have a negative impact on its robustness (its capacity to run without crashes.)

---

@title[General principles]

## General Principles of Software Quality

(Borrowed from "Code Complete, 2nd Edition" by Steve McConnell, Microsoft Press)

---

@title[…about cost]

> Improving quality reduces development costs.

---

@title[…compared to debugging]

> The single biggest activity on most projects is debugging and correcting code that does not work properly.

---

@title[…about time]

> Software defect removal is actually the most expensive and time-consuming form of work for software.

---

@title[…about time and defects]

> It is not necessarily the case than writing software without defects takes more time than writing software with defects. (…) it can take less.

---

@title[…about coverage]

> Testing by itself does not improve software quality. Test results are an indicator of quality, but in an of themselves they do not improve it. **Trying to improve software quality by increasing the amount of testing is like trying to lose weight by weighing yourself more often.**

---

@title[Techniques]

## Quality Management Techniques

- Testing
    - Unit
    - Integration
    - "System" or "End-to-end"
- Defensive programming
- Code reviews
- …

Note: There are several kinds of quality management techniques for software projects, but we are going to just concentrate on one.

---

@title[Here we go:]

## Unit Tests

Simplest, cheapest, fastest, first step in quality assurance.

---

## Definition:

Making a module do something, and then *asserting* that it has done exactly what we wanted.

---

## Genesis 1/2

> The origins of these frameworks actually started in Smalltalk. Kent Beck was a big fan of automated testing at the heart of software development.

[Martin Fowler](https://www.martinfowler.com/bliki/Xunit.html)

---

## Genesis 2/2

> JUnit was born on a flight from Zurich to the 1997 OOPSLA in Atlanta. Kent was flying with Erich Gamma, and what else were two geeks to do on a long flight but program?

[Martin Fowler](https://www.martinfowler.com/bliki/Xunit.html)

---

## Characteristics

- Written as `(void) => void` methods…
- …themselves organized into classes |
- Run by a "test runner", either CLI or IDE |
- setUp() and tearDown() |
- Use an assertion library (usually part of framework) |
- Test fails if one of the assertions fails |

---

## Examples

Note: In the following slides I've gathered some examples of unit tests in some projects I have worked, both here in Vilea in the past three months, and in a [personal project of mine.](https://github.com/akosma/conway)

---

@title[Ruby test]

```ruby
require 'test_helper'

class TestSuite < ActiveSupport::TestCase

  test "can sum one plus two" do
    value = 1 + 2
    assert_equal 3, value
  end

end
```

Note: This is the simplest unit test you can imagine. We perform a calculation, and we assert that the result is what we expected. Trivial, certainly, but unit tests are essentially this: execute and assert.

---

@title[Swiss Flymap-Windows]

```csharp
[TestClass]
public class SeatmapTest
{
    [TestMethod]
    public void SerializeSimpleMap()
    {
        var list = new List<Cell>();
        list.Add(new BorderCell())
            .Add(new EmptyCell())
            .Add(new GalleyCell())
            .Add(new SeatCell())
            .Add(new ToiletCell());

        var matrix = new List<List<Cell>>();
        matrix.Add(list);
        Seatmap seatmap = new Seatmap(matrix);

        string result = SeatmapManager.encodeSeatmap(seatmap, false);

        string expected = @"{""matrix"":[[{""type"":""border"",""label"":""65"",""visible"":true,""properties"":[""yeah"",""boom""]},{""type"":""inner"",""subtype"":""empty""},{""type"":""inner"",""subtype"":""galley""},{""type"":""inner"",""subtype"":""seat"",""seatRef"":""1A"",""klass"":""first"",""free"":false,""properties"":[""one"",""two"",""three"",""four""],""occupationStatus"":"""",""paxStatus"":"""",""fClassConnection"":false,""extraFlags"":false},{""type"":""inner"",""subtype"":""toilet""}]]}";

        Assert.AreEqual(expected, result);
    }
}
```

Note: These tests come from the Swiss "flypad-windows" project, and they helped me in several ways: 1. to get to know the project. I had a separate playground to dump code and see how to build objects and use them, without breaking the main application. 2. as a way to ensure that the code I was writing was doing what I wanted it to do. The idea was to generate a JSON string that must be given to the seatmap component, itself written in TypeScript, HTML, CSS and SVG. By making sure that the JSON generated is correct, we have a testbed for solving problems in the future.

---

@title[Swyx CSTA Implementation]

```swift
import XCTest

class CSTAIncomingMessageFactoryTests: XCTestCase {
    let factory = CSTAIncomingMessageFactory()

    func testThrowsOnUnknownResponseXML() {
        let str = "<note><to>John</to><from>Jane</from><heading>Reminder</heading><body>Don't forget the bread</body></note>"
        let data = str.data(using: .utf8)!

        XCTAssertThrowsError(try factory.buildMessage(for: data))
    }
}
```

Note: Sometimes you need to test that a component throws an exception in your code. Most frameworks include assertions for code that throws and code that must not throw exceptions. In this case I have a component that must create a native object out of an XML data stream, corresponding to a protocol called CSTA. Here I pass some arbitrary XML, it should be parsed successfully as a raw XML DOM object, but then there is no CSTA-specific object for this XML, so it would throw an exception to notify clients about this. In this case we could even assert the type of the exception to make things even more explicit.

---

@title[Swiss Flymap-Windows]

```csharp
[TestMethod]
public void SortSeats()
{
    string[] disorderedSeats = { "10A", "40C", "8A", "20A", "1B", "1A", "2B", "2A", "10B", "1C", "20B", "9A", "3C" };
    List<SeatCell> seats = new List<SeatCell>();
    foreach (string seatRef in disorderedSeats)
    {
        var seat = new SeatCell();
        seat.SeatRef = seatRef;
        seats.Add(seat);
    }

    string[] orderedSeats = (from seat in seats
                             orderby seat.SeatRef.PadLeft(5)
                             select seat.SeatRef).ToArray<string>();
    string[] expected = { "1A", "1B", "1C", "2A", "2B", "3C", "8A", "9A", "10A", "10B", "20A", "20B", "40C" };

    CollectionAssert.AreEqual(expected, orderedSeats);
}
```

Note: I added this test after solving a small puzzle with Laurence; how to order seats properly? Well, add padding spaces in front of them, and then just sort them; the end result appears exactly how one would. The test makes the assumption obvious, about how seat ordering works in the app.

---

@title[Swiss Flymap-Windows]

```csharp
[TestMethod]
public void SerializeScheduleInter()
{
    ScheduleInter schedule = new ScheduleInter();
    NewFormatWriter writer = new NewFormatWriter();
    string soap = writer.Serialize(schedule, true);

    Assert.IsNotNull(soap);
}
```

Note: Sometimes you just need to make sure that the output of a method is not null; this is useful in languages that do not (yet) include optionals. This code comes from Swiss "CoreScenarioMigrator" project, which helps us migrate scenario files from one version to the next, every time the backend team at Swiss changes the web service. In this case we make sure that the output of our method is exactly what we expected.

---

@title[Swiss Seatmap Engine]

Load JSON file:

```typescript
declare module "*.json" {
    const value: any;
    export default value;
}
```

And then:

```typescript
import * as A380_SEATMAP from 'A380.json';

// ...
obj.doSomething(A380_SEATMAP);
```

Note: Sometimes you need to load raw JSON in JavaScript projects, to simulate some big response from a server; you can do that directly from TypeScript using these commands, so that your object gets the data it needs to do something interesting. This allows developers to load a sample JSON file into the HTML seatmap component, which can be useful during development. We use "dependency injection" here in the sense that the seatmap component is oblivious to how the data comes from: a database, a network call, a file; we just inject it and expect the seatmap to appear in all its glory on the screen of our app.

---

@title[Swyx CSTA Implementation]

```swift
class CSTAManagerTests: XCTestCase {
    var manager: CSTAManagerProtocol = CSTAManager()
    var mockModule: CSTAModuleProtocol = CSTAModuleMock()

    override func setUp() {
        manager = CSTAManager()
        mockModule = CSTAModuleMock()
        manager.cstaModule = mockModule
    }

    func testCannotSendMessageBeforeStarting() {
        let failure = expectation(description: "Cannot do anything before calling `start()`")
        failure.expectedFulfillmentCount = 5 // expectedFulfillmentCount starts at 1...
        mockModule.failureExpectation = failure

        manager.monitorStart(device: device)
        manager.makeCall(device: device, callingNumber: "456")
        manager.monitorStop(monitorID: monitorID)
        manager.stop()

        wait(for: [failure], timeout: 1)
    }
}
```

Note: This is again dependency injection at work. The manager object requires a module object, both referenced by their respective interfaces, not by their class names. By doing so we can compose objects and for testing, we can provide a "mock" implementation, which simulates a complete interaction with a server. In this case the server uses a rather strict protocol, so the mock helps us test the manager object and make sure it does what we need it to do. The test makes explicit the sequence of steps required to use the object properly.

---

@title[Catch with C++]

```c++
#define CATCH_CONFIG_MAIN

#include <vector>
#include "catch.hpp"

#include "coord.h"
#include "world.h"

using namespace conway;

TEST_CASE("Block")
{
    auto alive = World::block(Coord(0, 0));
    std::shared_ptr<World> original(new World(5, alive));
    auto next = original->evolve();
    REQUIRE(*next == *original);
}
```

Note: Catch is a C, C++ and Objective-C unit test framework, open source and cross-platform, distributed as a single header file. It can be embedded and compiled as part of a single file, and it generates an executable that does nothing but execute the tests. Due to the lack of runtime type inspection in C and C++, all unit test frameworks targeting them must use macros to define the tests and the assertions. CppUnit and other frameworks do the same. Catch can also be used in BDD style.

---

@title[BDD vs TDD]

## BDD === TDD

They are just the same thing. Execute and assert.

---

## BDD vs TDD 1/2

```kotlin
package training.akosma.conway

import org.junit.Assert.assertEquals
import org.junit.Test

class TubPatternTests {
    @Test
    fun isStable() {
        val alive = World.tub(Coord(0, 0))
        val original = World(5, alive)
        val next = original.evolve()
        assertEquals(original, next)
    }
}
```

Note: In the traditional style, we execute and assert. The title of the test says what it is supposed to do. If only one assertion fails, the test fails. It is quite imperative but it does the job.

---

## BDD vs TDD 2/2

```typescript
import { World } from "../src/World";
import { Coord } from "../src/Coord";
import { expect } from 'chai';
import { describe, it } from 'mocha';

describe("The Tub pattern", () => {
    it("is stable", () => {
        let alive = World.tub(new Coord(0, 0));
        let original = new World(5, alive);
        let next = original.evolve();
        expect(original).to.equal(next);
    });
});
```

Note: In the BDD style, we "describe" our modules in terms of things they can do: "it can calculate square roots," "it applies VAT," etc. It is easier to read by non-developers, so it's preferred as a style of communication with other stakeholders. But they also execute and assert. There is absolutely no difference in using one or the other, it's just a matter of: 1. style, 2. availability of a BDD framework in your tech of choice.

---

## Creating Testable Code 1/2

- Code to interfaces, never to real class names
- Isolate objects from one another |
- Make small methods |
- Isolate branch statements as much as possible |
- Use the standard testing framework of your technology |

Note: The term "standard testing framework" might or might not be the default framework provided by the vendor.

---

## Creating Testable Code 2/2

- No Singletons. Seriously.
- Pass dependencies as public properties (easier than with constructors) |
- Use dependency injection containers (for example, Ionic's) |
- Inject data providers: database, network, etc. |

Note: We should start rejecting code that has singletons in our peer reviews. They are handy to use but a nightmare to test, because they cannot be easily replaced at runtime with other mock objects with the same interface.

---

## What To Test?

- TypeScript removes the need for type checks in tests (required with pure JavaScript)
- "Business rules" are a natural candidate
- Classes without I/O
- Manager objects overseeing communication protocols, or stateful interactions
- Data transformation classes

Note: The classes that lead themselves more easily to testing are those "pure functions" kind of code where there is just an input and an output, without side effects like files, databases or network connections. Any exploration of unit testing must start there, and then little by little move to realms with side effects. Dependency injection is the next step after that.

---

## Dependency Injection

- Via properties (easier) or via constructor (like Ionic)
- Start with properties |
- Types of properties should be interfaces & protocols, never concrete types |
- At testing time, set the properties you need with mock objects implementing those interfaces |
- **AVOID. SINGLETONS.** Enough said. |

Note: No code runs in void. All code has dependencies, and to be able to test classes in isolation you need to isolate objects from one another. Testing fully fledged objects can be complicated: database connections, network connections, files, etc... all that makes code more complex to test. And yes, this means that for each class you want to test in the system, a myriad of interfaces will have to be created. It pays off in the future.

---

## Running Tests 1/2

Android

```bash
$ ./gradlew test
```

iOS / macOS

```bash
$ xcodebuild -scheme Project \
     -destination 'platform=iOS Simulator,name=iPhone 8' test | xcpretty
```

TypeScript / JavaScript

```bash
$ npm test
```

Note: This are the command line tools that you can use when configuring your CI systems (in our case, Gitlab) so that tests are executed automatically after each successful build.

---

## Running Tests 2/2

Windows

```
$ mstest.exe /testcontainer:library.dll
$ vstest.console.exe /Platform:x64 file.appx /InIsolation
```

Ruby on Rails

```
$ bin/rails test
```

---

@title[Xcode 9]

![](assets/images/xcode.png)

Note: Xcode shows exactly the location where a test failed. You can also click the "play" button in the gutter and debug a single test (add a breakpoint, you're done). Hence it is important that each test is isolated from others, without shared state. Never share state among tests. In Swift, tests are `(void) => void` functions that begin with the word "test".

---

@title[Visual Studio for Windows 2017]

![](assets/images/vs_win.png)

Note: Pay attention to the fact that Windows Store and Universal Windows applications are signed, and as such test targets cannot be run with the simpler `mstest.exe` program, but rather with the `vstest.console.exe` one. This requires the testing target to be signed with a valid PFX file, which can be cumbersome, but currently there is no other solution. It also takes quite long to run. Also, Visual Studio for Windows allows for the creation of "playlists" to just execute a certain number of tests if needed. In .NET, unit tests are delimited with special attributes.

---

@title[Visual Studio for Mac 7.4]

![](assets/images/vs_mac.png)

Note: Surprisingly good support and fast execution. I recommend this environment to play with unit tests simply and fast. It supports both xUnit and MSTest, the two most important unit testing frameworks in the .NET world.

---

@title[Visual Studio Code, March 2018]

![](assets/images/vs_code.png)

Note: There is a Mocha sidebar helper available for VS Code, but unfortunately I could not make it run with my current version of Mocha while preparing these slides. The best solution, as for other languages, it to use the command-line runners, either inside or outside of VS Code.

---

@title[Android Studio 3.1]

![](assets/images/android.png)

Note: Very good support for Kotlin and Java tests. Can run one or all tests, as required. Just as with .NET languages, JVM unit tests are specified with attributes provided by JUnit.

---

## Test Coverage

- Evil brother of unit tests
- Can be useful to spot problems
- If it becomes an obsession, stop

Note: Code Coverage is a generic term for the collection of information about the portions of code that are "exercised" with unit tests. Some IDEs show that information directly on the UI (for example Xcode and Android Studio.) Some organizations become obsessed with it, and try to reach 100% coverage. The truth is, Pareto's law applies here: 80% of the benefits of unit testing are brought by testing just 20% of the codebase.

---

@title[Coverage in Xcode 9]

![](assets/images/coverage_xcode.png)

---

@title[Configuring coverage in Xcode 9]

![](assets/images/coverage_xcode_conf.png)

---

@title[Coverage in Android Studio 3.1]

![](assets/images/coverage_android.png)

---

## Visual Studio

On Windows, oly allows code coverage in the "Enterprise" edition

```
$ vstest.console.exe library.dll /EnableCodeCoverage
```

On Mac, no support yet for .NET Core. [Minicover](https://github.com/lucaslorentz/minicover) could be an option.

---

## Side Effects of Unit Testing

- Better APIs with live documentation
- Decoupled, modular designs with smaller classes and less, isolated branch statements |
- Increased development speed with less defects |
- Trust in your own code |
- Avoid regression bugs |
- Find problems early |
- Continuous integration |
- Easier "inboarding" of new team members |

Note: Unit testing is a very good exercise in that it forces the developer to think their APIs as they move forward. Nice, readable APIs are a joy to use, and the best ones around have been created with unit tests. As for inboarding, they are great: "checkout the code, run the tests, everything should be green, use the tests as doc, let me know if you have questions."

---

## Recommended

- One unit test class per component
- Write short tests |
- Run them continuously |
- Use Dependency Injection |
- Do not persist state between tests |
- Good tests execute fast |
- Unit testing classes with business logic without I/O works great |
- Use SQLite / Realm in-memory databases |

Note: Each test must be independent; one **must** be able to run them separately. They must execute fast. Should run often, hopefully automatically. CI systems can do this for us automatically. You can use a tool like `watch` to run tests continuosly in your command line.

---

## Complicated

- Networking / asynchronous tests
- Mocking objects |
- Complex objects with lots of methods |
- Methods with lots of branch statements |
- Cross-platform |
- Setting up test suites |

Note: Dynamic languages such as JavaScript, Ruby and Objective-C have better support for "mock objects" than other strictly typed languages; with a dynamic language, one can discover the interface of an object at runtime and build a mock object out of thin air. As for methods with a high number of branch statements, they have "higher cyclomatic complexity" and each possible path should be covered by a test.

---

## Avoid

Unless really needed:

- Performance tests
- Integrated testing
- UI testing
- Aiming for 100% code coverage

Note: Testing the lower levels of a system is always better; the foundation of an application must be rock solid. The UI, on the other hand, can change so quickly that it becomes it very hard for the test code to keep up and stay updated all the time.

---

## Tools & Frameworks

- .NET: [MSUnit](https://docs.microsoft.com/en-us/visualstudio/test/unit-test-your-code), [xUnit](https://xunit.github.io/)
- TypeScript: [tape](https://github.com/substack/tape), [Jasmine](https://jasmine.github.io/), [Mocha](https://mochajs.org/), [Chai](http://www.chaijs.com/)
- Objective-C: [XCTest](https://developer.apple.com/documentation/xctest), [Kiwi](https://github.com/kiwi-bdd/Kiwi)
- Swift: [XCTest](https://github.com/apple/swift-corelibs-xctest), [Quick](https://github.com/Quick/Quick), [Nimble](https://github.com/Quick/Nimble)
- JVM: [JUnit](https://junit.org/junit5/)
- C++: [Catch 2](https://github.com/catchorg/Catch2)
- Command line: [xcpretty](https://github.com/supermarin/xcpretty), [xctool](https://github.com/facebook/xctool), [msbuild](https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild), [vstest.console](https://msdn.microsoft.com/en-us/library/ms182486.aspx), [gradlew](https://docs.gradle.org/current/userguide/gradle_wrapper.html), [TAP](https://testanything.org/)

Note: The Catch library for C/C++ is contained in a single header, and can be integrated easily in any project, even in Objective-C projects.

---

## Books

---

@title[Working Effectively with Legacy Code]

![](assets/images/legacy_code.jpg)

[Working Effectively with Legacy Code](https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052) by Michael Feathers

---

@title[Code Complete, 2nd Edition]

![](assets/images/code_complete.jpg)

[Code Complete, 2nd Edition](https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670) by Steve McConnell

---

## Now It's Your Turn

Note: It would be good for all of us to start using Unit Testing slowly but surely, adding tests to existing apps, making hidden assumptions visible. This will help us to understand other people's code more easily, and to speed up the resolution of bugs, to have nicer APIs and better designs. But then, when to do that?

---

## Henry Miller

![width=100](assets/images/henrymiller.jpg)

> When you can't **create**, you can **work**.

---

## Working 1/2

- Peer code reviews & design meetings
- Adding API documentation
- Adding assertions
- Adding accessibility features

Note: "Adding assertions" refers to using `console.assert()` and `assert()` statements in our debugging code, to remove them in production builds (this can be done easily with Webpack and any compiler can do it) so that we can document in our code the pre- and post-conditions of our methods and classes. This will make our code easier to follow and it will fail earlier and faster, which is always good.

---

## Working 2/2

- Adding tests to existing code
- Adding tests when fixing bugs
- Adding tests to write new, complex code
- Running the tests with unorthodox data
- Refactoring existing apps to make them testable

Note: These are some ideas about where to add testing in our daily workflow, as we move in new features and work fixing bugs.

---

## Thanks!

Note: I invite you to play with unit tests with the programming language of your choice, start with any project you're working on: 1. install a testing target, setup the tools and 2. find suitable objects to add tests for.
