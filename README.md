# Presentation about Unit Testing

This repository contains a presentation given April 13th, 2018.

The file `PITCHME.md` can be viewed using [GitPitch](https://gitpitch.com/), [mdp](https://github.com/visit1985/mdp) or [Deckset](https://www.decksetapp.com/).

When using [GitPitch Desktop](https://gitpitch.com/desktop), just use the `./run.sh` script to launch the presentation, and open a browser at <http://localhost:9000/> to view its contents.
